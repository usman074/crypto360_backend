const _ = require('lodash');
const axios = require('axios');

exports.SanitizeApiRequest = (request) => {
    try {
        let data = _.pick(request, ['invoice_id', 'amount', 'merchant_id', 'currency', 'api_key', 'callback_url']);
        let len = _.keys(data).length;
        if (len !== 6) {
            throw new Error('Data doesn\'t contain all required attributes')
        } else {
            if (_.isString(data.merchant_id) && _.isString(data.invoice_id) && _.isString(data.currency) && _.isString(data.api_key) && _.isString(data.callback_url)) {

                if (typeof (data.amount) == "number") {
                    return true;
                }
                else {
                    throw new Error("Amount must be an integer")
                }
            }
            else {
                throw new Error("Merchant Id, Currency and Invoice Id must be a string")
            }
        }
    } catch (error) {
        throw new Error(error);
    }
}

exports.BTC = async (currency) => {

    try {
        const prices = await axios.get('https://blockchain.info/de/ticker');
        var value = prices.data[currency].last;
        return value;
    } catch (error) {
        throw new Error(error);
    }
}

exports.AmountInBTC = (value, amount) => {
    var price = (1 / value) * amount;
    var resulted_price = price.toString().substr(0, 10);
    var truncated_price = parseFloat(resulted_price, 10);
    return truncated_price;
}
