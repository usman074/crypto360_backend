const Sequelize = require('sequelize');
const ApiKey = require('../models/api-keys')(sequelize, Sequelize.DataTypes);

exports.addApiKey = async (request) => {
    try {
        const apiKeyCount = await ApiKey.count({ where: { merchant_id: request.merchant_id } });
        if (apiKeyCount === 0) {
            const apiKey = await ApiKey.create({
                merchant_id: request.merchant_id
            })
            return apiKey;
        } else if (apiKeyCount === 1) {
            throw new Error('Api key already generated');
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.getApiKey = async (request) => {
    try {
        const apiKey = await ApiKey.findOne({ where: { merchant_id: request.merchant_id } });
        console.log(apiKey)
        return apiKey
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.delApiKey = async (request) => {
    try {
        const apiKey = await ApiKey.destroy(
            { where: { merchant_id: request.merchant_id } });
        return apiKey;
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

