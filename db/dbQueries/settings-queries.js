const Sequelize = require('sequelize');
const merchantBitcoinDetail = require('../models/merchant-botcoin-detail')(sequelize, Sequelize.DataTypes);
const Merchant = require('../models/merchant-profile')(sequelize, Sequelize.DataTypes);
const md5 = require('md5');



exports.getMerchantProfile = async (request) => {
    try {
        const merchantProfile = await Merchant.findOne({
            attributes: ['merchant_id', 'firstName', 'lastName', 'email', 'organizationName'],
            where: {
                merchant_id: request.merchant_id
            }
        });
        return merchantProfile
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.updateMerchantProfile = async (request, user) => {
    try {
        const merchantProfile = await Merchant.update({
            firstName: request.firstName,
            lastName: request.lastName,
            organizationName: request.organizationName
        }, {
            where: { merchant_id: user.merchant_id }
        })
        return merchantProfile
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.updatePassowrd = async (request, user) => {
    try {
        const password = await Merchant.findOne({
            attributes: ['password'],
            where: { merchant_id: user.merchant_id }
        });
        if (md5(request.currentPassword) === password.dataValues.password) {
            const newPassword = await Merchant.update({
                password: md5(request.newPassword)
            }, {
                where: { merchant_id: user.merchant_id }
            })
            return newPassword;
        } else {
            throw new Error('Current password is incorrect')
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.addXpubkey = async (request, user) => {
    try {
        const xpubkey = await merchantBitcoinDetail.findOne({
            attributes: ['xpubkey'],
            where: { merchant_id: user.merchant_id }
        });
        console.log(xpubkey);
        if (xpubkey) {
            const xpub = await merchantBitcoinDetail.update({
                xpubkey: request.xpubkey
            }, {
                where: { merchant_id: user.merchant_id }
            })
            return xpub;
        } else {
            const xpub = await merchantBitcoinDetail.create({
                xpubkey: request.xpubkey,
                merchant_id: user.merchant_id
            });
            return xpub;
        }

    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.getXpubkey = async (request) => {
    try {
        const xpubkey = await merchantBitcoinDetail.findOne({
            attributes: ['xpubkey'],
            where: {
                merchant_id: request.merchant_id
            }
        });
        return xpubkey
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}
