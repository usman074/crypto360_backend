const Sequelize = require('sequelize');
const Merchant = require('../models/merchant-profile')(sequelize, Sequelize.DataTypes);
const md5 = require('md5');
const {v3: uuidv3} = require('uuid');

exports.signUp = async (request) => {
    try {
        const hashedPassword = md5(request.password);

        const merchant = await Merchant.create({
            email: request.email,
            password: hashedPassword,
            firstName: request.firstName,
            lastName: request.lastName,
            organizationName: request.organizationName
        });
        return merchant;
    } catch (error) {
        console.log("Error ", error)
        const errorMessage = error.errors ? error.errors[0].message : error;
        if (errorMessage === 'email must be unique') {
            throw new Error("Email Already Registered");
        } else if (errorMessage === 'Validation isEmail on email failed') {
            throw new Error("Email is not valid");
        } else {
            throw new Error("Something went wrong");
        }
    }
}

exports.logIn = async (request) => {
    try {
        const hashedPassword = md5(request.password);
        const merchant = await Merchant.findOne({
            attributes: ['merchant_id', 'firstName', 'lastName', 'verified'],
            where: { email: request.email, password: hashedPassword }
        });
        if (merchant) {
            return merchant;
        } else {
            throw new Error('Incorrect email / password')
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        if (errorMessage === 'email must be unique') {
            throw new Error("Email Already Registered");
        } else if (errorMessage === 'Validation isEmail on email failed') {
            throw new Error("Email is not valid");
        } else {
            throw new Error(error);
        }
    }
}

exports.verifyAccount = async (merchant_id, authorization_token) => {
    try {
        const merchant = await Merchant.findOne({
            attributes: ['verified'],
            where: { merchant_id, authorization_token }
        });
        if (merchant) {
            if (merchant.verified) {
                throw new Error('Account already verified')
            } else {
                const verified = await Merchant.update({
                    verified: true
                }, {
                    where: { merchant_id, authorization_token }
                })
                return verified;
            }
        } else {
            throw new Error('Invalid Link')
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.GetResendEmailDetails = async (request) => {
    try {
        const merchant = await Merchant.findOne({
            attributes: ['merchant_id', 'authorization_token', 'email'],
            where: { email: request.email }
        });
        if (merchant) {
            return merchant;
        } else {
            throw new Error('Incorrect Email');
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.ForgetPassDetails = async (request) => {
    try {
        const merchant = await Merchant.findOne({
            attributes: ['merchant_id', 'password', 'email'],
            where: { email: request.email }
        });
        if (merchant) {
            return merchant;
        } else {
            throw new Error('Incorrect Email');
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.VerifyResetDetails = async (merchant_id, resetId) => {
    try {
        const merchant = await Merchant.findOne({
            attributes: ['merchant_id', 'password'],
            where: { merchant_id }
        });
        if (merchant) {
            var response = merchant.merchant_id + merchant.password;
            var data = uuidv3(response, uuidv3.DNS);
            data = data.replace(/-/g, "");
            if (resetId == data) {
                return true;
            } else {
                throw new Error('Invalid Link');
            }
        } else {
            throw new Error('Invalid Link');
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.ResetMerchantPassword = async (request, merchant_id) => {
    try {
        const hashedPassword = md5(request.password);
        const passwordUpdated = await Merchant.update({
            password: hashedPassword
        }, {
            where: { merchant_id }
        })
        return passwordUpdated
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}