const Sequelize = require('sequelize');
const merchantBitcoinDetail = require('../models/merchant-botcoin-detail')(sequelize, Sequelize.DataTypes);
const paymentDetail = require('../models/payment-detail')(sequelize, Sequelize.DataTypes);
const bitcoin = require("bitcoinjs-lib");
const config = require('../../config/btc-config');
var network = (config.testnet) ? bitcoin.networks.testnet : bitcoin.networks.bitcoin;

exports.GenerateAddress = async (request) => {

    try {
        const addressValues = await merchantBitcoinDetail.findOne({
            attributes: ['xpubkey', 'indexValue'],
            where: {
                merchant_id: request.merchant_id
            }
        });
        if (addressValues) {
            let xpubString = bitcoin.bip32.fromBase58(addressValues.xpubkey, network).derivePath("0/" + addressValues.indexValue);
            const { address } = bitcoin.payments.p2pkh({
                pubkey: xpubString.publicKey,
                network: network
            })
            console.log(address)
            addressValues.indexValue++;
            await updateMerchantIndex(addressValues.indexValue, request.merchant_id);
            return address
        } else {
            throw new Error('Xpub Key not Found')
        }
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.addPaymentRecord = async (request, address) => {
    try {
        var date = new Date(Date.now());
        const payment = await paymentDetail.create({
            address: address,
            invoice_id: request.invoice_id,
            amount: request.amount,
            callback_url: request.callback_url,
            Date: date.toLocaleString(),
            merchant_id: request.merchant_id
        });
        return payment;

    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.GetUnusedPaymentaddresses = async ()=> {
    try {
        const unUsedAddresses = await paymentDetail.findAll({ where: {Unused_Addresses: 1} });
        return unUsedAddresses;

    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.GetRecordByAddress = async (address)=> {
    try {
        const paymentRecord = await paymentDetail.findOne({ where: {address} });
        return paymentRecord;

    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.UpdatePaymentRecord = async (paymentRecord) => {
    try {
        const updatedPaymentRecord = await paymentDetail.update({
            tx_timestamp: paymentRecord.tx_timestamp,
            tx_id: paymentRecord.tx_id,
            Unused_Addresses: paymentRecord.Unused_Addresses,
            confirmations: paymentRecord.confirmations,

        }, {
            where: { address: paymentRecord.address }
        })
        return updatedPaymentRecord;
    } catch (error) {
        throw new Error(error)
    }
}

async function updateMerchantIndex(index, merchant_id) {
    try {
        const updatedIndex = await merchantBitcoinDetail.update({
            indexValue: index
        }, {
            where: { merchant_id: merchant_id }
        })
        return updatedIndex;
    } catch (error) {
        throw new Error('Index not updated')
    }
}