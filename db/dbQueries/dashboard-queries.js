const Sequelize = require('sequelize');
const Op = Sequelize.Op
const paymentDetail = require('../models/payment-detail')(sequelize, Sequelize.DataTypes);

exports.GetRevenueDetails = async (user, conf, date) => {
    try {
        const revenue = await paymentDetail.findAll({
            attributes: ['amount', 'Date'],
            where: {
                merchant_id: user.merchant_id,
                confirmations: { [Op.gte]: conf },
                Date: { [Op.gte]: date }
            }
        });
        return revenue
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}