const Sequelize = require('sequelize');
const Op = Sequelize.Op
const paymentDetail = require('../models/payment-detail')(sequelize, Sequelize.DataTypes);

exports.GetUnresolvedPaymentDetails = async (user, conf) => {
    try {
        const unresolvedPayments = await paymentDetail.findAll({
            where: {
                merchant_id: user.merchant_id,
                confirmations: { [Op.lt]: conf },
            }
        });
        return unresolvedPayments
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.GetPaidPaymentDetails = async (user, conf) => {
    try {
        const paidPayments = await paymentDetail.findAll({
            where: {
                merchant_id: user.merchant_id,
                confirmations: { [Op.gte]: conf }
            }
        });
        return paidPayments;
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}

exports.GetAllPaymentDetails = async (user) => {
    try {
        const allPayments = await paymentDetail.findAll({
            where: {
                merchant_id: user.merchant_id,
            }
        });
        return allPayments
    } catch (error) {
        const errorMessage = error.errors ? error.errors[0].message : error;
        throw new Error(errorMessage);
    }
}