const randomstring = require('randomstring');
const {v3: uuidv3} = require('uuid');
require('../conn');
module.exports = (sequelize, DataTypes) => {
    var api_key = sequelize.define('api_key', {
        api_key: {
            type: DataTypes.UUID,
            allowNull: false,
            unique: true
        },
        merchant_id: DataTypes.UUID
        
    },
    {
        hooks: {
            beforeValidate: (api, option) => {
                var apiKey = uuidv3(randomstring.generate(), uuidv3.DNS);
                apiKey = apiKey.replace(/-/g, "");
                api.api_key = apiKey;
            }
        }
    });

    api_key.associate = models => {
        api_key.belongsTo(models.merchant_profile);
    }

    return api_key;
}
