const randomstring = require('randomstring');
const {v3: uuidv3} = require('uuid');
require('../conn');
module.exports = (sequelize, DataTypes) => {
    var merchant_profile = sequelize.define('merchant_profile', {
        merchant_id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        firstName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        organizationName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        verified: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        },
        password: {
            type: DataTypes.STRING(),
            allowNull: false,
            validate: {
                len: [8, 50]
            }
        },
        authorization_token: {
            type: DataTypes.STRING(100),
            allowNull: false,
        }
    },
        {
            hooks: {
                beforeValidate: (merchant, option) => {
                    var merchantId = uuidv3(randomstring.generate(), uuidv3.DNS);
                    merchantId = merchantId.replace(/-/g, "");
                    merchant.merchant_id = merchantId;
                    merchant.authorization_token = randomstring.generate();
                }
            }
        });

    merchant_profile.associate = models => {
        merchant_profile.hasOne(models.api_key);
        merchant_profile.hasOne(models.merchant_bitcoin_detail);
        merchant_profile.hasOne(models.payment_detail);
    }

    return merchant_profile;
}
