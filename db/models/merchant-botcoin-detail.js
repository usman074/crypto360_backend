const randomstring = require('randomstring');
const {v3: uuidv3} = require('uuid');
require('../conn');
module.exports = (sequelize, DataTypes) => {
    var merchant_bitcoin_detail = sequelize.define('merchant_bitcoin_detail', {
        bitcoin: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 1
        },
        xpubkey: {
            type: DataTypes.STRING(),
            allowNull: false,
        },
        indexValue: {
            type: DataTypes.INTEGER(),
            allowNull: false,
            defaultValue: 0
        },
        merchant_id: {
            type: DataTypes.UUID,
            unique: true
        }
    });

    merchant_bitcoin_detail.associate = models => {
        merchant_bitcoin_detail.belongsTo(models.merchant_profile);
    }

    return merchant_bitcoin_detail;
}
