const randomstring = require('randomstring');
const {v3: uuidv3} = require('uuid');
require('../conn');
module.exports = (sequelize, DataTypes) => {
    var payment_detail = sequelize.define('payment_detail', {
        payment_id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true
        },
        address: {
            type: DataTypes.STRING(),
            allowNull: false,
        },
        tx_timestamp: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        invoice_id: {
            type: DataTypes.STRING(),
            allowNull: false,
        },
        amount: {
            type: DataTypes.FLOAT,
            allowNull: false,
        },
        tx_id: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        Unused_Addresses: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        confirmations: {
            type: DataTypes.INTEGER(),
            allowNull: false,
            defaultValue: 0
        },
        Date: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        Coin: {
            type: DataTypes.STRING(),
            allowNull: false,
            defaultValue: 'BTC'
        },
        callback_url: {
            type: DataTypes.STRING(),
            allowNull: false,
        },
        merchant_id: DataTypes.UUID

    },
        {
            hooks: {
                beforeValidate: (paymentDetail, option) => {
                    var paymentId = uuidv3(randomstring.generate(), uuidv3.DNS);
                    paymentId = paymentId.replace(/-/g, "");
                    paymentDetail.payment_id = paymentId;
                }
            }
        });

    payment_detail.associate = models => {
        payment_detail.belongsTo(models.merchant_profile);
    }

    return payment_detail;
}
