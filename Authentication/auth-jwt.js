var jwt = require('jsonwebtoken');
var secret='secretSignatutre'
var sign=function(merchant){
    var token = jwt.sign({exp: Math.floor(Date.now() / 1000) + (3600), merchant: merchant},secret );
    return token;
}
var verify=function(token){
  return new Promise(function(resolve,reject){
    if (!token) {
      reject("Authentication Failed");
    }
    else{
      jwt.verify(token, secret, function(error, res) {
              if (error)
              {
                reject(error);
              }
              else{
                resolve(res.merchant)
              }
      });
    }
  });
}

module.exports = {
   sign: sign,
   verify:verify,
};