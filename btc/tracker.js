'use strict'
/** 
 * It will get the unused addresses and find UTXO's aginst the addresses
*/
// ===========================================
// Load Modules
// ===========================================
const dbQueryBtc = require('../db/dbQueries/btc-api-queries');
const _ = require("lodash");
const config = require('../config/btc-config');
const network = config.testnet ? config.TESTNET : config.LIVENET
const confirmations = config[network]['confirmations']
const mailer = require('../utils/mailer');
const axios = require('axios');
const one_satoshi = 0.00000001;
const Bitcoin3 = require("bitcoin3")
const btc3 = new Bitcoin3(network)

let slidingFactor = 0; //increase recursive interval 
const slidingInterval = 15000; // 15 seconds
const slidingIntervalLimit = 600000; // 10 minutes
const pollLimit = 30000; // tracker interval 


// wait for db to load before initiating tracker
setTimeout(() => {
    tracker();
}, 2000)


let processContribution = async (item, address) => {
        var result = await dbQueryBtc.GetRecordByAddress(address);
        if (!_.isEmpty(result)) {
            let updated = false;
            var amount = item.satoshis * one_satoshi;
            if (amount >= result.amount) {
                if (result.tx_timestamp === null) {
                    updated = true;
                    result.tx_timestamp = item.timestamp ? item.timestamp : Date.now()
                }
                if (result.tx_id === null) {
                    updated = true;
                    result.tx_id = item.txid ? item.txid : null
                }
                if (item.confirmations >= confirmations) {
                    result.Unused_Addresses = false;
                    result.confirmations = item.confirmations;
                    updated = true;
                }
                else {
                    updated = false;
                }
            }
            if (updated) {
                try {
                    const paymentUpdated = await dbQueryBtc.UpdatePaymentRecord(result);
                    console.log('Record Updated');
                    axios.get(result.callback_url + "&order_id=" + result.invoice_id).then(() => {
                        console.log("Sent callback to the merchant for payment confirmation.");
                        // const html = 'Hi there, <br/> Your payment ' + result.amount + ' BTC aginst order ' + result.invoice_id + ' has been received and confirmed successfully.<br>Thankyou!';
                        // var mailOptions = {
                        //     from: mailer.email,
                        //     to: queryResponse.email,
                        //     subject: 'Please Verify Your Email Address',
                        //     html
                        //   };
                        // mailer.sendEmail("admin@crypto360.com", temp_result[0].user_email, "Your Payment has been Received", html).then((info) => {
                        //     console.log("Email to user has been sent.");
                        // }).catch((err) => {
                        //     console.log("Error trying to send email to the user for payment confirmation.", err);
                        // });
                    })
                        .catch(error => {
                            console.log("Error trying to send callback to the merchant for payment confirmation.", error);
                        });
                } catch (error) {
                    console.log('Unexpected error occurred while trying to update contribution', error);

                }
            }

        }
        else if (_.isEmpty(result)) {
            console.log('Couldn\'t get record from db against %s:', item.address);
        }
}
/**
 * @dev function to go through the given addresses and fetch any deposit on success the deposit details are forwaded to [/txwatch]  
 * @param {array} addresses
 */
let trackContributions = async (addresses) => {
    // get utxos for limit number of addresses at a time
    // console.log(addresses[0].address)

    _.forEach(addresses, (addr) => {
        // console.log(addr.address);
        btc3.getUtxo(addr.address)
        .then((utxo) => {
            // console.log(utxo)
            processContribution(utxo[0], addr.address);
        })
        .catch((err) => {
            console.log('error getting utxos of ', addr.address);
        })
    })
}

/**
 * @dev recursive function with sliding interval to put a watcher on  BTC addresses.
 * Every recursive call duration is increased by 15 seconds and max limit is 30 min.
 */
let tracker = async () => {

    let time,

        unusedAddresses = await dbQueryBtc.GetUnusedPaymentaddresses();
    // console.log(unusedAddresses)
    // console.log(unusedAddresses.length)
    let length = unusedAddresses.length == 0 ? 1 : unusedAddresses.length;
    if (!_.isEmpty(unusedAddresses)) {
        // to preserve FIFO queue
        console.log('%d unused addresses found. Trying to find contributions.', length)
        trackContributions(unusedAddresses);
        slidingFactor = 0; //reset sliding counter
        time = pollLimit + (300 * length); // add 3ms delay per the number of addresses to be tracked

    } else {
        // increase 30 seconds in recursive calls if there is no btc address
        console.log('No unused addresses found');
        let interval = (slidingFactor + slidingInterval);
        slidingFactor = (interval <= slidingIntervalLimit) ? interval : slidingIntervalLimit;
        time = (length * pollLimit) + slidingFactor;
    }

    console.log('Will track again after %d seconds', time / 1000)

    //recursive call
    setTimeout(tracker, time);
};
