const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/api-key-queries');

router.post('/', async function (req, res) {
    try {

        const queryResponse = await dbQuery.addApiKey(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/', async function (req, res) {
    try {

        const queryResponse = await dbQuery.getApiKey(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.delete('/', async function (req, res) {
    try {

        const queryResponse = await dbQuery.delApiKey(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});



module.exports = router;