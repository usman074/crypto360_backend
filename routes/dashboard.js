const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/dashboard-queries');
const config = require('../config/btc-config');
router.get('/', async function (req, res) {
    try {

        var date = new Date(Date.now() - 2592000000);
        var conf = config.testnet ? config.testnet.confirmations : config.livenet.confirmations;
        const queryResponse = await dbQuery.GetRevenueDetails(req.user, conf, date);

        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

module.exports = router;