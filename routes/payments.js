const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/payments-queries');
const config = require('../config/btc-config');

router.get('/all', async function (req, res) {
    try {
        const queryResponse = await dbQuery.GetAllPaymentDetails(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/paid', async function (req, res) {
    try {
        var conf = config.testnet ? config.testnet.confirmations : config.livenet.confirmations;
        const queryResponse = await dbQuery.GetPaidPaymentDetails(req.user, conf);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/unresolved', async function (req, res) {
    try {
        var conf = config.testnet ? config.testnet.confirmations : config.livenet.confirmations;
        const queryResponse = await dbQuery.GetUnresolvedPaymentDetails(req.user, conf);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

module.exports = router;