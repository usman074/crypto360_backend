const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/user-auth-queries');
const jwt = require('../Authentication/auth-jwt');
const mailer = require('../utils/mailer');
const config = require('../config/btc-config');
const {v3: uuidv3} = require('uuid');

router.post('/signup', async function (req, res) {
    try {
        const queryResponse = await dbQuery.signUp(req.body);
        const html = 'Hi there, <br/> Thankyou for registering! <br/><br/> Please verify your email by clicking on the provided link <br/> link <a href ="' + config.CLIENT_BASE_URL + '/verify/email/' + queryResponse.merchant_id + '/' + queryResponse.authorization_token + '"> Click here to verify </a>';
        var mailOptions = {
            from: mailer.email,
            to: queryResponse.email,
            subject: 'Please Verify Your Email Address',
            html
        };
        mailer.sendMail(mailOptions)
            .then((mailResponse) => {

                res.statusCode = 200;
                res.json(
                    {
                        response: mailResponse,
                        success: true,
                        message: null
                    }
                )
                res.end();
            })
            .catch((err) => {
                res.statusCode = 400;
                res.json(
                    {
                        response: null,
                        success: false,
                        message: 'Something went wrong'
                    }
                )
                res.end();
            })
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.post('/login', async function (req, res) {
    try {
        const queryResponse = await dbQuery.logIn(req.body);
        if (queryResponse && queryResponse.verified) {
            const token = jwt.sign(queryResponse);
            res.statusCode = 200;
            res.json(
                {
                    response: queryResponse,
                    token: token,
                    success: true,
                    message: null
                }
            )
            res.end();
        } else {
            throw new Error('Account is not verified');
        }
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/verify/:merchantId/:AuthToken', async function (req, res) {
    try {
        const queryResponse = await dbQuery.verifyAccount(req.params.merchantId, req.params.AuthToken);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                token: null,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json({
            response: null,
            success: false,
            message: error.message
        }
        )
        res.end();
    }
});

router.post('/resendEmail', async function (req, res) {
    try {
        const queryResponse = await dbQuery.GetResendEmailDetails(req.body);
        const html = 'Hi there, <br/> Thankyou for registering! <br/><br/> Please verify your email by clicking on the provided link <br/> link <a href ="' + config.BASE_URL + '/user/auth/verify/' + queryResponse.merchant_id + '/' + queryResponse.authorization_token + '">' + config.BASE_URL + '/user/auth/verify/' + queryResponse.merchant_id + '/' + queryResponse.authorization_token + '</a>';
        var mailOptions = {
            from: mailer.email,
            to: queryResponse.email,
            subject: 'Please Verify Your Email Address',
            html
        };
        await mailer.sendMail(mailOptions)
        res.statusCode = 200;
        res.json(
            {
                response: 'Account verification email sent successfully',
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.post('/forgotPassword', async function (req, res) {
    try {
        const queryResponse = await dbQuery.ForgetPassDetails(req.body);
        var resetId = queryResponse.merchant_id + queryResponse.password;
        var resetId = uuidv3(resetId, uuidv3.DNS);
        resetId = resetId.replace(/-/g, "");
        const html = `Hi there, <br/><br/> Please click on the provided link to reset your password <br/><a href ="${config.CLIENT_BASE_URL}/reset/${queryResponse.merchant_id}/${resetId}">Click here to reset your password.</a><br><br>If you have not requested the password reset link then ignore this email.`;
        var mailOptions = {
            from: mailer.email,
            to: queryResponse.email,
            subject: 'Please click on link to reset password',
            html
        };
        await mailer.sendMail(mailOptions)
        res.statusCode = 200;
        res.json(
            {
                response: 'Password reset link sent successfully. please check your email.',
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/resetPass/:merchantId/:resetId', async function (req, res) {
    try {
        const queryResponse = await verifyPasswordResetLink(req.params.merchantId, req.params.resetId);
        if (queryResponse) {
            res.statusCode = 200;
            res.json(
                {
                    response: 'Link validated',
                    success: true,
                    message: null
                }
            )
            res.end();
        } else {
            throw new Error('Something went wrong');
        }
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.put('/resetPass/:merchantId/:resetId', async function (req, res) {
    try {
        const queryResponse = await verifyPasswordResetLink(req.params.merchantId, req.params.resetId);
        if (queryResponse) {
            if (req.body.password === req.body.confirmPassword) {
                const passwordUpdated = await dbQuery.ResetMerchantPassword(req.body, req.params.merchantId)
                res.statusCode = 200;
                res.json(
                    {
                        response: passwordUpdated,
                        success: true,
                        message: null
                    }
                )
                res.end();
            } else {
                throw new Error('New and Confirm Password mismatch');
            }
        } else {
            throw new Error('Something went wrong');
        }
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

async function verifyPasswordResetLink(merchantId, resetId) {
    try {
        const response = await dbQuery.VerifyResetDetails(merchantId, resetId);
        return response;
    } catch (error) {
        throw new Error(error);
    }
}



module.exports = router;