const express = require('express');
const router = express.Router();
const dbQueryApi = require('../db/dbQueries/api-key-queries');
const dbQueryBtc = require('../db/dbQueries/btc-api-queries');
const btcUtils = require('../utils/btc-api-utils');

router.post('/payment', async function (req, res) {
    try {
        const dataValidate = await btcUtils.SanitizeApiRequest(req.body);
        console.log(dataValidate);
        if (dataValidate) {
            const value = await btcUtils.BTC(req.body.currency);
            const btcAmount = await btcUtils.AmountInBTC(value, req.body.amount);
            req.body.amount = btcAmount;
            console.log(req.body)
            const apiKey = await dbQueryApi.getApiKey(req.body);
            if (apiKey) {
                if (apiKey.api_key === req.body.api_key) {
                    console.log("validated")
                    const address = await dbQueryBtc.GenerateAddress(req.body);
                    const paymentDetail = await dbQueryBtc.addPaymentRecord(req.body, address)
                    res.statusCode = 200;
                    res.json({
                        paymentDetail,
                        success: true,
                        message: null
                    })
                    res.end()
                } else {
                    throw new Error('Invalid Api Key');
                }
            } else {
                throw new Error('No key Against merchant Id ', req.body.merchantId);
            }
        }
    }
    catch (error) {
        console.log(error.message)
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});




module.exports = router;