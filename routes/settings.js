const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/settings-queries');

router.get('/profile', async function (req, res) {
    try {

        const queryResponse = await dbQuery.getMerchantProfile(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.put('/profile', async function (req, res) {
    try {

        const queryResponse = await dbQuery.updateMerchantProfile(req.body, req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.put('/password', async function (req, res) {
    try {
        if (req.body.newPassword === req.body.confirmPassword) {
            const queryResponse = await dbQuery.updatePassowrd(req.body, req.user);
            res.statusCode = 200;
            res.json(
                {
                    response: queryResponse,
                    success: true,
                    message: null
                }
            )
            res.end();
        } else {
            throw new Error('New and Confirm Password mismatch');
        }
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.post('/xpubkey', async function (req, res) {
    try {
        const queryResponse = await dbQuery.addXpubkey(req.body, req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

router.get('/xpubkey', async function (req, res) {
    try {
        const queryResponse = await dbQuery.getXpubkey(req.user);
        res.statusCode = 200;
        res.json(
            {
                response: queryResponse,
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});

module.exports = router;