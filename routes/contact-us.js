const express = require('express');
const router = express.Router();
const mailer = require('../utils/mailer');

router.post('/', async function (req, res) {
    try {
        var mailOptions = {
            from: mailer.email,
            to: mailer.email,
            subject: req.body.subject,
            html: `From: ${req.body.email} <br> ${req.body.message}`
        };
        await mailer.sendMail(mailOptions)
        res.statusCode = 200;
        res.json(
            {
                response: 'Email has been sent. We will get back to you soon.',
                success: true,
                message: null
            }
        )
        res.end();
    }
    catch (error) {
        res.statusCode = 400;
        res.json(
            {
                response: null,
                success: false,
                message: error.message
            }
        )
        res.end();
    }
});


module.exports = router;