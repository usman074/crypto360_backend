const express = require("express");
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require("body-parser");
const userAuth = require('./routes/user-auth');
const ApiKey = require('./routes/api-key');
const Settings = require('./routes/settings');
const btcApi = require('./routes/btc-api');
const dashboard = require('./routes/dashboard');
const payments = require('./routes/payments');
const contactUs = require('./routes/contact-us');
const path = require('path');
const fs = require("fs");
const jwt = require('./Authentication/auth-jwt');
require('./db/conn');
require('./btc/tracker');
const morgan = require("morgan");

const app = express()
const http = require('http').createServer(app);
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(helmet())

// ----------------------------------------
// CORS
// ----------------------------------------

app.use(cors({ origin: '*' }));

app.use('/user/auth', userAuth);
app.use('/btc', btcApi);
app.use('/contactUs', contactUs);

app.use((req, res, next) => {
    const token = req.headers["x-access-token"];
    jwt.verify(token).then((response) => {
        req.user = response;
        next();
    }).catch((error) => {
        console.log(error)
        res.statusCode = 400;
        res.json({ error });
        res.end();
    });
})
app.use('/api', ApiKey);
app.use('/settings', Settings);
app.use('/dashboard', dashboard);
app.use('/payments', payments);

app.get('/test', (req, res) => {
    console.log('helo')
    res.end();
})



http.listen(3000, () => {
    console.log("Server is listening on port 3000");
})