'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('payment_details', {
      payment_id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true
      },
      address: {
        type: Sequelize.STRING(),
        allowNull: false,
      },
      tx_timestamp: {
        type: Sequelize.STRING(),
        allowNull: true,
      },
      invoice_id: {
        type: Sequelize.STRING(),
        allowNull: false,
      },
      amount: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      tx_id: {
        type: Sequelize.STRING(),
        allowNull: true,
      },
      Unused_Addresses: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      confirmations: {
        type: Sequelize.INTEGER(),
        allowNull: false,
        defaultValue: 0
      },
      Date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      Coin: {
        type: Sequelize.STRING(),
        allowNull: false,
        defaultValue: 'BTC'
      },
      callback_url: {
        type: Sequelize.STRING(),
        allowNull: false,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('payment_details');
  }
};
