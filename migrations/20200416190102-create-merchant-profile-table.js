'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('merchant_profiles', {
      merchant_id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true
    },
    email: {
        type: Sequelize.STRING(100),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true
      }
    },
    firstName: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    organizationName: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    verified: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: 0
    },
    password: {
        type: Sequelize.STRING(),
        allowNull: false,
        validate: {
          len: [8, 50]
      }
    },
    authorization_token: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('merchant_profiles');
  }
};
