'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('merchant_bitcoin_details', {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      bitcoin: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        default: 1
      },
      xpubkey: {
        type: Sequelize.STRING(),
        allowNull: false,
      },
      indexValue: {
        type: Sequelize.INTEGER(),
        allowNull: false,
        default: 0
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('merchant_bitcoin_details');
  }
};
