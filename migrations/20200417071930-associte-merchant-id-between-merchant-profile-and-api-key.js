'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'api_keys', // name of Source model
      'merchant_id', // name of the key we're adding 
      {
        type: Sequelize.UUID,
        references: {
          model: 'merchant_profiles', // name of Target model
          key: 'merchant_id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'api_keys', // name of Source model
      'merchant_id' // key we want to remove
    );
  }
};
