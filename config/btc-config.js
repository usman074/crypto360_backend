module.exports = {
    testnet: true,
    livenet:
    {
        confirmations: 6,
    },
    testnet: {
        confirmations: 1,
    },
    TESTNET: 'testnet',
    LIVENET: 'livenet',
    timeout: 20000,
    BASE_URL: 'http://localhost:3000',
    CLIENT_BASE_URL: 'http://localhost:4200'
}